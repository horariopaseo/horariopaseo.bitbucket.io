var calendarioDni = {
    "0": {
        "lunes": "9:00 a 9:20",
        "martes": "11:30 a 11:50",
        "miercoles": "14:00 a 14:20",
        "jueves": "16:30 a 16:50",
        "viernes": "19:00 a 19:20",
        "sabado": "12:30 a 12:50",
        "domingo": "16:30 a 16:50"
    },
    "4": {
        "lunes": "9:30 a 9:50",
        "martes": "12:00 a 12:20",
        "miercoles": "14:30 a 14:50",
        "jueves": "17:00 a 17:20",
        "viernes": "19:30 a 19:50",
        "sabado": "13:00 a 13:20",
        "domingo": "17:00 a 17:20"
    },
    "8": {
        "lunes": "10:00 a 10:20",
        "martes": "12:30 a 12:50",
        "miercoles": "15:00 a 15:20",
        "jueves": "17:30 a 17:50",
        "viernes": "20:00 a 20:20",
        "sabado": "13:30 a 13:50",
        "domingo": "17:30 a 17:50"
    },
    "12": {
        "lunes": "10:30 a 10:50",
        "martes": "13:00 a 13:20",
        "miercoles": "15:30 a 15:50",
        "jueves": "18:00 a 18:20",
        "viernes": "20:30 a 20:50",
        "sabado": "14:00 a 14:20",
        "domingo": "18:00 a 18:20"
    },
    "16": {
        "lunes": "11:00 a 11:20",
        "martes": "13:30 a 13:50",
        "miercoles": "16:00 a 16:20",
        "jueves": "18:30 a 18:50",
        "viernes": "21:00 a 21:20",
        "sabado": "14:30 a 14:50",
        "domingo": "18:30 a 18:50"
    },
    "20": {
        "lunes": "11:30 a 11:50",
        "martes": "14:00 a 14:20",
        "miercoles": "16:30 a 16:50",
        "jueves": "19:00 a 19:20",
        "viernes": "9:00 a 9:20",
        "sabado": "15:00 a 15:20",
        "domingo": "19:00 a 19:20"
    },
    "24": {
        "lunes": "12:00 a 12:20",
        "martes": "14:30 a 14:50",
        "miercoles": "17:00 a 17:20",
        "jueves": "19:30 a 19:50",
        "viernes": "9:30 a 9:50",
        "sabado": "15:30 a 15:50",
        "domingo": "19:30 a 19:50"
    },
    "28": {
        "lunes": "12:30 a 12:50",
        "martes": "15:00 a 15:20",
        "miercoles": "17:30 a 17:50",
        "jueves": "20:00 a 20:20",
        "viernes": "10:00 a 10:20",
        "sabado": "16:00 a 16:20",
        "domingo": "20:00 a 20:20"
    },
    "32": {
        "lunes": "13:00 a 13:20",
        "martes": "15:30 a 15:50",
        "miercoles": "18:00 a 18:20",
        "jueves": "20:30 a 20:50",
        "viernes": "10:30 a 10:50",
        "sabado": "16:30 a 16:50",
        "domingo": "20:30 a 20:50"
    },
    "36": {
        "lunes": "13:30 a 13:50",
        "martes": "16:00 a 16:20",
        "miercoles": "18:30 a 18:50",
        "jueves": "21:00 a 21:20",
        "viernes": "11:00 a 11:20",
        "sabado": "17:00 a 17:20",
        "domingo": "21:00 a 21:20"
    },
    "40": {
        "lunes": "14:00 a 14:20",
        "martes": "16:30 a 16:50",
        "miercoles": "19:00 a 19:20",
        "jueves": "9:00 a 9:20",
        "viernes": "11:30 a 11:50",
        "sabado": "17:30 a 17:50",
        "domingo": "9:00 a 9:20"
    },
    "44": {
        "lunes": "14:30 a 14:50",
        "martes": "17:00 a 17:20",
        "miercoles": "19:30 a 19:50",
        "jueves": "9:30 a 9:50",
        "viernes": "12:00 a 12:20",
        "sabado": "18:00 a 18:20",
        "domingo": "9:30 a 9:50"
    },
    "48": {
        "lunes": "15:00 a 15:20",
        "martes": "17:30 a 17:50",
        "miercoles": "20:00 a 20:20",
        "jueves": "10:00 a 10:20",
        "viernes": "12:30 a 12:50",
        "sabado": "18:30 a 18:50",
        "domingo": "10:00 a 10:20"
    },
    "52": {
        "lunes": "15:30 a 15:50",
        "martes": "18:00 a 18:20",
        "miercoles": "20:30 a 20:50",
        "jueves": "10:30 a 10:50",
        "viernes": "13:00 a 13:20",
        "sabado": "19:00 a 19:20",
        "domingo": "10:30 a 10:50"
    },
    "56": {
        "lunes": "16:00 a 16:20",
        "martes": "18:30 a 18:50",
        "miercoles": "21:00 a 21:20",
        "jueves": "11:00 a 11:20",
        "viernes": "13:30 a 13:50",
        "sabado": "19:30 a 19:50",
        "domingo": "11:00 a 11:20"
    },
    "60": {
        "lunes": "16:30 a 16:50",
        "martes": "19:00 a 19:20",
        "miercoles": "9:00 a 9:20",
        "jueves": "11:30 a 11:50",
        "viernes": "14:00 a 14:20",
        "sabado": "20:00 a 20:20",
        "domingo": "11:30 a 11:50"
    },
    "64": {
        "lunes": "17:00 a 17:20",
        "martes": "19:30 a 19:50",
        "miercoles": "9:30 a 9:50",
        "jueves": "12:00 a 12:20",
        "viernes": "14:30 a 14:50",
        "sabado": "20:30 a 20:50",
        "domingo": "12:00 a 12:20"
    },
    "68": {
        "lunes": "17:30 a 17:50",
        "martes": "20:00 a 20:20",
        "miercoles": "10:00 a 10:20",
        "jueves": "12:30 a 12:50",
        "viernes": "15:00 a 15:20",
        "sabado": "21:00 a 21:20",
        "domingo": "12:30 a 12:50"
    },
    "72": {
        "lunes": "18:00 a 18:20",
        "martes": "20:30 a 20:50",
        "miercoles": "10:30 a 10:50",
        "jueves": "13:00 a 13:20",
        "viernes": "15:30 a 15:50",
        "sabado": "9:00 a 9:20",
        "domingo": "13:00 a 13:20"
    },
    "76": {
        "lunes": "18:30 a 18:50",
        "martes": "21:00 a 21:20",
        "miercoles": "11:00 a 11:20",
        "jueves": "13:30 a 13:50",
        "viernes": "16:00 a 16:20",
        "sabado": "9:30 a 9:50",
        "domingo": "13:30 a 13:50"
    },
    "80": {
        "lunes": "19:00 a 19:20",
        "martes": "9:00 a 9:20",
        "miercoles": "11:30 a 11:50",
        "jueves": "14:00 a 14:20",
        "viernes": "16:30 a 16:50",
        "sabado": "10:00 a 10:20",
        "domingo": "14:00 a 14:20"
    },
    "84": {
        "lunes": "19:30 a 19:50",
        "martes": "9:30 a 9:50",
        "miercoles": "12:00 a 12:20",
        "jueves": "14:30 a 14:50",
        "viernes": "17:00 a 17:20",
        "sabado": "10:30 a 10:50",
        "domingo": "14:30 a 14:50"
    },
    "88": {
        "lunes": "20:00 a 20:20",
        "martes": "10:00 a 10:20",
        "miercoles": "12:30 a 12:50",
        "jueves": "15:00 a 15:20",
        "viernes": "17:30 a 17:50",
        "sabado": "11:00 a 11:20",
        "domingo": "15:00 a 15:20"
    },
    "92": {
        "lunes": "20:30 a 20:50",
        "martes": "10:30 a 10:50",
        "miercoles": "13:00 a 13:20",
        "jueves": "15:30 a 15:50",
        "viernes": "18:00 a 18:20",
        "sabado": "11:30 a 11:50",
        "domingo": "15:30 a 15:50"
    },
    "96": {
        "lunes": "21:00 a 21:20",
        "martes": "11:00 a 11:20",
        "miercoles": "13:30 a 13:50",
        "jueves": "16:00 a 16:20",
        "viernes": "18:30 a 18:50",
        "sabado": "12:00 a 12:20",
        "domingo": "16:00 a 16:20"
    }
};

function onDniClick(event) {
  var cifrasDni = parseInt(document.getElementById("dni").value);
  if (isNaN(cifrasDni) || cifrasDni<0) {
    document.getElementById("lunes").textContent = "cifras de DNI incorrectas";
    document.getElementById("martes").textContent = "cifras de DNI incorrectas";
    document.getElementById("miercoles").textContent = "cifras de DNI incorrectas";
    document.getElementById("jueves").textContent = "cifras de DNI incorrectas";
    document.getElementById("viernes").textContent = "cifras de DNI incorrectas";
    document.getElementById("sabado").textContent = "cifras de DNI incorrectas";
    document.getElementById("domingo").textContent = "cifras de DNI incorrectas";
  } else {
    var grupoDni = calendarioDni[cifrasDni % 100 - cifrasDni % 4] ;
    for (let key in grupoDni) {
      document.getElementById(key).textContent = grupoDni[key];
    }
    
  }
	
}

document.getElementById("ver").addEventListener("click", onDniClick);

